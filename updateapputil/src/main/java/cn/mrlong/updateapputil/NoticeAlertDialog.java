package cn.mrlong.updateapputil;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class NoticeAlertDialog extends Dialog {
    public interface Callback {
        void callbackSure();

        void callbackCancel();
    }

    Callback callback;
    private TextView content;
    private TextView sureBtn;
    private TextView cancleBtn;

    public NoticeAlertDialog(Context context, Callback callback) {
        super(context, R.style.CustomDialog);
        this.callback = callback;
        setCustomDialog();
    }

    private void setCustomDialog() {
        View mView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_notice, null);
        sureBtn = (TextView) mView.findViewById(R.id.dialog_confirm_sure);
        cancleBtn = (TextView) mView.findViewById(R.id.dialog_confirm_cancle);
        content = (TextView) mView.findViewById(R.id.dialog_confirm_title);


        sureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.callbackSure();
                NoticeAlertDialog.this.cancel();
            }
        });
        cancleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.callbackCancel();
                NoticeAlertDialog.this.cancel();
            }
        });
        super.setContentView(mView);
    }


    public NoticeAlertDialog setContent(String s) {
        content.setText(s);
        return this;
    }

    public NoticeAlertDialog showView() {
        this.show();
        return this;
    }
}
