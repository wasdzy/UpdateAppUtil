package cn.mrlong.updateapputil;

import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    String url = "http://117.78.36.57:36582/clients/android/iamvt_v2_67.apk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button tv_btn = findViewById(R.id.tv_btn);
        Button tv_btn2 = findViewById(R.id.tv_btn2);
        Button tv_btn3 = findViewById(R.id.tv_btn3);
        tv_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateAppUtils.from(MainActivity.this)
                        .setApkUrl(url)
                        .initXutils3(getApplication())
                        .setServerVersionCode(2)
                        .setUpdateInfo("这里更新！")
                        .start();
            }
        });
        tv_btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateAppUtils.from(MainActivity.this)
                        .setApkUrl(url)
                        .setDownLoadByType(UpdateAppUtils.DOWNLOAD_BY_BROWSER)
                        .setServerVersionCode(2)
                        .setUpdateInfo("")
                        .start();
            }
        });
        tv_btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateAppUtils.from(MainActivity.this)
                        .initXutils3(getApplication())
                        .setApkUrl(url)
                        .setDownLoadByType(UpdateAppUtils.DOWNLOAD_BY_BROADCAST)
                        .setServerVersionCode(2)
                        .setUpdateInfo("")
                        .start();
            }
        });
    }
}
